<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <!-- Styles -->
        <link href="/css/app.css" rel="stylesheet">
        <script>
            window.Laravel =  {!!json_encode([
                'csrfToken' => csrf_token(),
            ])!!}
        </script>
    </head>
    <body>
        <div id="app">
            @include('partials.nav')
            <router-view></router-view>
        </div>
        <script src="/js/app.js"></script>
		<script>
			if ('serviceWorker' in navigator) {
			  navigator.serviceWorker.register('/service-worker.js').then(function() { 
				console.log("Service Worker Registered"); 
			  });
			}
		</script>
    </body>
</html>
