import VueRouter from 'vue-router';

let routes=[
    {
        path:'/',
        component:require('./components/Tasks')
    },
    {
        path:'/persons',
        component:require('./components/Persons')
    },
    {
        path:'/persons/create',
        component:require('./components/PersonCreate')
    },
    {
        path:'/tasks/create',
        component:require('./components/TaskCreate'),
        meta: { mode: 'create' }
    },
    {
        path:'/tasks/:id/edit',
        component:require('./components/TaskCreate'),
        meta: { mode: 'edit' }
    },
    {
        path:'/persons/:id/edit',
        component:require('./components/PersonCreate'),
        meta: { mode: 'edit' }
    },
];

export default new VueRouter({
	mode: 'history',
    routes
});