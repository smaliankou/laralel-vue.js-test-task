<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/


/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Person::class, function (Faker\Generator $faker) {
    $faker->addProvider(new Faker\Provider\ru_RU\Person($faker));
    $sex = $faker->randomElement(['Male', 'Female']);
    return [
        'first_name' => $faker->{'firstName'.$sex},
        'second_name' => $faker->lastName . ($sex == 'Female'? 'а': ''),
        'middle_name' => $faker->{'middleName'.$sex},
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Task::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->numerify('Task ###'),
        'volume' => $faker->numberBetween(0, 200),
        'date_start' => $faker->dateTime,
        'date_end' => $faker->dateTime,
        'person_id' => function () {
            return App\Person::inRandomOrder()->first()->id;
        },
        'status_id' => function () {
            return App\Status::inRandomOrder()->first()->id;
        }
    ];
});
