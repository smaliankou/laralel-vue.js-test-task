module.exports = {
  staticFileGlobs: [
    'public/css/**.css',
    'public/**.html',
    'public/images/**.*',
    'public/js/**.js'
  ],
  stripPrefix: 'public/',
  runtimeCaching: [{
    urlPattern: '/',
    handler: 'networkFirst'
  },
  {
    urlPattern: /\/api\/.*/,
    handler: 'networkFirst'
  }
  ]
};