<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    public $timestamps = false;

    protected $guarded = ['id'];

    /**
     * Get the person record associated with the task.
     */
    public function person()
    {
        return $this->belongsTo(Person::class);
    }

    /**
     * Get the status record associated with the task.
     */
    public function status()
    {
        return $this->belongsTo(Status::class);
    }
}
