<?php

namespace App\Http\Controllers;

use App\Http\Requests\TaskRequest;
use App\Person;
use App\Status;
use App\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Task::with(['person', 'status'])->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $statuses = Status::all();
        $persons = Person::all();
        return response()->json(compact(['statuses', 'persons'], 201));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param TaskRequest|Request $request
     * @return string
     */
    public function store(TaskRequest $request)
    {
        Task::create($request->all());
        return 'success';
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        $statuses = Status::all();
        $persons = Person::all();
        return response()->json(compact(['statuses', 'persons', 'task']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param TaskRequest|Request $request
     * @param  \App\Task $task
     * @return string
     */
    public function update(TaskRequest $request, Task $task)
    {
        $task->update($request->all());
        return response()->json([
            'updated' => true
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        $task->delete();
        return response()->json([
                'deleted' => true
        ]);
    }
}
